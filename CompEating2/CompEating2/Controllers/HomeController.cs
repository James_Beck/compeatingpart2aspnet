﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace CompEating2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Joey()
        {
            return View("~/Views/Home/Joey.cshtml");
        }

        public IActionResult Carmen()
        {
            return View("~/Views/Home/Carmen.cshtml");
        }

        public IActionResult Matt()
        {
            return View("~/Views/Home/Matt.cshtml");
        }

        public IActionResult Geoffrey()
        {
            return View("~/Views/Home/Geoffrey.cshtml");
        }

        public string TestString()
        {
            return "This is a test string to check that the controller is working";
        }
    }
}
